import { User } from './User';


export interface Elevator {
    elevatorId: string;
    status: string;
    lastUpdated: string;

    user: User;
}
