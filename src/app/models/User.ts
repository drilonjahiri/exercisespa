export interface User {
    id: string;
    FirstName: string;
    LastName: string;
    DateOfBirth: Date;
    UserName: string;
    Password: string;
}
