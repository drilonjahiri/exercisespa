import { Component, OnInit } from '@angular/core';
import { ElevatorService } from '../_services/elevator.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Elevator } from '../models/Elevator';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  elevators: Elevator[];

  constructor(public elevatorService: ElevatorService,
    private route: ActivatedRoute,
    private router: Router,
    public authService: AuthService) { }

  ngOnInit() {
    this.getAllElevators();
  }

  getAllElevators(): any {
    this.elevatorService.getAll().subscribe(data => {
      this.elevators = data;
    });
  }

}
