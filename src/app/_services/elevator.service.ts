import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Elevator } from '../models/Elevator';

@Injectable({
  providedIn: 'root'
})
export class ElevatorService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Elevator[]>(
      this.baseUrl + 'Elevator/GetAll');
  }

  get(id: string) {
    return this.http.get<Elevator>(this.baseUrl + 'Elevator/' + id);
  }
}
