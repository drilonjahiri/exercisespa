/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ElevatorService } from './elevator.service';

describe('Service: Elevator', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElevatorService]
    });
  });

  it('should ...', inject([ElevatorService], (service: ElevatorService) => {
    expect(service).toBeTruthy();
  }));
});
