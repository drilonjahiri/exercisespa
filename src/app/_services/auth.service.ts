import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { User } from '../models/User';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthUser } from '../models/AuthUser';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.apiUrl;
  userToken: any;
  decodedToken: any;
  currentUser: User;
  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService) { }

    login(model: any) {
      return this.http.post<AuthUser>(this.baseUrl + 'account/login', model,  {headers: new HttpHeaders()
        .set('Content-Type', 'application/json')})
        .map(user => {
        if (user) {
          localStorage.setItem('token', user.accessToken);
          localStorage.setItem('refreshToken', user.refreshToken);
          localStorage.setItem('user', JSON.stringify(user.user));
          this.decodedToken = this.jwtHelperService.decodeToken(user.accessToken);
          this.currentUser = user.user;
          this.userToken = user.accessToken;
        }
      });
    }

    loggedIn() {
      const token = this.jwtHelperService.tokenGetter();
      if (!token) {
        return false;
      }

      return !this.jwtHelperService.isTokenExpired(token);
    }
}
