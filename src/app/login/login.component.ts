import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { FormGroup, FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};

  user: User;
  registerForm: FormGroup;
  // tslint:disable-next-line:no-inferrable-types
  nextStep: boolean = false;
  hasBaseDropZoneOver = false;
  baseUrl = environment.apiUrl;

  constructor(
    public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {}

  ngOnInit() { }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertify.success('Logged in successfully');
      this.router.navigate(['/home']);
    }, error => {
      this.alertify.error('Ann error occured');
    }, () => {
      this.router.navigate(['/home']);
    });
  }


  loggedIn() {
    return this.authService.loggedIn();
  }
}
