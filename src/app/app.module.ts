import { AuthGuard } from './_guard/auth.guard';
import { appRoutes } from './routes';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDropdownModule,  PaginationModule, ButtonsModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http';

import { AuthService } from './_services/auth.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DatePipe } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { ElevatorService } from './_services/elevator.service';

export function HttpLoaderFactory(http: HttpClient) {}

export function getAccessToken(): string {
  return localStorage.getItem('token');
}

export const jwtConfig = {
  tokenGetter: getAccessToken
};

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      LoginComponent,
      NavComponent
   ],
   imports: [
      BrowserModule,
      HttpModule,
      FormsModule,
      BsDropdownModule.forRoot(),
      RouterModule.forRoot(appRoutes),
      ReactiveFormsModule,
      PaginationModule.forRoot(),
      ButtonsModule.forRoot(),
      HttpClientModule,
      JwtModule.forRoot({
         config: {
           tokenGetter: getAccessToken,
           whitelistedDomains: ['localhost:51422'],
           blacklistedRoutes: ['localhost:51422/api/login', 'localhost:51422/api/refreshToken']
         }
       }, )
   ],
  providers: [
    AuthService,
    AuthGuard,
    DatePipe,
    ElevatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
