import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { User } from './models/User';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor ( private authService: AuthService, private jwtHelperService: JwtHelperService) { }

 // tslint:disable-next-line:use-life-cycle-interface
 ngOnInit() {
  const token = localStorage.getItem('token');
  const user: User = JSON.parse(localStorage.getItem('user'));
  if (token) {
    this.authService.decodedToken = this.jwtHelperService.decodeToken(token);
  }

  if (user) {
    this.authService.currentUser = user;
  }
 }
}
